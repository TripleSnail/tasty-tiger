var trains = {};
var stations = {};


function init()
{
    fillStationList();    
}

function fillTrainsList()
{
    var allTrainsXML = requestAllTrainsXML();
    var parser = new DOMParser();
    var allTrainsDOM = parser.parseFromString(allTrainsXML, "text/xml");
    var allTrains = allTrainsDOM.getElementsByTagName("item");
    
    for (var k in allTrains)
    {
        var item = allTrains[k];
        var category = getElementValueById(item, "cat");
        var trainguid = getElementValueById(item, "guid");

        if (category == "H")
            trains[trainguid] = {
                guid: trainguid,
                title: getElementValueById(item, "title"),
                from: getElementValueById(item, "from"),
                to: item.getElementValueById(item ,"to")
            };
    }
    printLocalTrains();
}
