function addStation(code, fullName) 
{
    var stationBox = document.forms['box']['station'];  
    stationBox.options[stationBox.options.length] = new Option(fullName, code);   
}

function fillStationList() 
{
    var stationCodesLen = Object.keys(stationCodes).length;
    for (var k in stationCodes)
        addStation(stationCodes[k], k);
}

function toInfoBox(data, captionStr)
{
    var asid = document.getElementsByTagName("aside")[0];
    var pos = [d3.event.pageX, d3.event.pageY];
    var infoTbl = document.createElement("table");

    infoTbl.setAttribute("id", "info");
    asid.setAttribute("style", "left:"+(pos[0] - 70) + "px ;top:"+(pos[1]-60)+"px;");
    
    for (var k in data)
    {
        var row = document.createElement("tr");
        var cellHeader = document.createElement("th");
        var cellInfo = document.createElement("td");
        var cellHeaderTextNode = document.createTextNode(k);
        var cellInfoTextNode = document.createTextNode(data[k]);

        cellHeader.appendChild(cellHeaderTextNode);
        cellInfo.appendChild(cellInfoTextNode);
        row.appendChild(cellHeader);
        row.appendChild(cellInfo);
        infoTbl.appendChild(row);
    }
    asid.appendChild(infoTbl);
}

function killInfoBox()
{
    var asid = document.getElementsByTagName("aside")[0];
    asid.removeChild(document.getElementById("info"));
}