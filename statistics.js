function calculateMaxLateness(trains)
{
    var maxLateness = 0;
    var trainsLen = Object.keys(trains).length;

    for (var t in trains)
        if (trains[t].lateness > maxLateness)
            maxLateness = trains[t].lateness;   

    return maxLateness;
}

function calculateAverageLateness(trains)
{
    var totalLateness = 0;
    var trainsLen = Object.keys(trains).length;
    for (var t in trains)
        totalLateness += trains[t].lateness;

    return (totalLateness / trainsLen);     
} 

function calculateMaxAverage(trainLetters) 
{
    var max = 0;
    for (var t in trainLetters)
    {
        if (trainLetters[t].latenessAverage > max)
            max = trainLetters[t].latenessAverage;
    } 
    return max;
}