function Station(stationInfo)
{
    this.sInfo = stationInfo;
    this.title = getElementValueById(this.sInfo, "title");
    console.log(this.title);
    this.trains = {};
    this.trainLetters = {};
    this.latenessInfo = {"late":{}, "!late":{}};
    var trainsD = this.sInfo.getElementsByTagName("item");
    var tLen = trainsD.length;

    for (var i = 0; i < tLen; i++)
    {
        var train = trainsD[i];
        var guid =  getElementValueById(train, "guid");
        var category = getElementValueById(train, "cat");
        var letter = getElementValueById(train, "title");

        if (category == "H" && letter.length == 1)
        {
            var lateness = parseInt(getElementValueById(train, "lateness"));
            this.trains[guid] = {
                guid: guid,
                title: letter,
                from: getElementValueById(train, "fromStation"),
                to: getElementValueById(train, "toStation"),
                lateness: lateness
                };

            if (lateness != 0)
                this.latenessInfo["late"][guid] = this.trains[guid];
            else
                this.latenessInfo["!late"][guid] = this.trains[guid];
            
            console.log("Title: " + this.trains[guid].title + ", Lateness: " + this.trains[guid].lateness);

            if (!(letter in this.trainLetters))
                this.trainLetters[letter] = {
                    title: letter,
                    count: 1,
                    latenessSum: lateness,
                    latenessAverage: lateness
                };   

            else 
            {
                var trainLetter = this.trainLetters[letter];
                trainLetter.count += 1;
                trainLetter.latenessSum += this.trains[guid].lateness;
                trainLetter.latenessAverage = Math.round(trainLetter.latenessSum / trainLetter.count);
            }
        
     
        }
    }
}

function printStationInfo()
{
    var station;
    var art = document.getElementsByTagName("article")[0];
    var code = document.forms['box']['station'].value;
    station = requestStationInfo(code);

    var stationLi = document.createElement("p");

    var stationLiText = station.title;
    var stationLiTextNode = document.createTextNode(stationLiText);
    
    stationLi.appendChild(stationLiTextNode);
    art.appendChild(stationLi);
   
    //stationLatenessesToBars(station);
    drawBars(station);
    stationLatenessToPie(station);
    var breakLine = document.createElement("br");
    art.appendChild(breakLine);
}

function stationLatenessToPie(station)
{
    
    var radius = 150;
    var svgHeight = 300;
    var svgWidth = 300;

    color = d3.scale.linear().domain([0,1]).range(["blue","red"]);

    data = [{"label":"in time", "value": Object.keys(station.latenessInfo["!late"]).length}, 
            {"label":"late", "value": Object.keys(station.latenessInfo["late"]).length}];


    var chart = d3.select("article")
        .append("svg:svg")
        .data([data])
        .attr("width", svgWidth)
        .attr("height", svgHeight)
        .append("svg:g")
        .attr("transform", "translate(" + radius + "," + radius + ")");
       
    var arc = d3.svg.arc()          
        .outerRadius(radius);

    var pie = d3.layout.pie()       
        .value(function(d) { return d.value; });

    var arcs = chart.selectAll("g.slice")
        .data(pie)
        .enter()
        .append("svg:g")
        .attr("class", "slice");
    

    arcs.append("svg:path")
        .attr("fill", function (d, i) {return color(i);})
        .attr("d", arc)
        .on("mouseover", function(d, i) {toInfoBox(data[i], data[i].label);})
        .on("mouseout", function(d) {killInfoBox();});

   
    arcs.append("svg:text")                    
        .attr("transform", function(d) {   
                  d.innerRadius = 0;
                  d.outerRadius = radius;
                  return "translate(" + arc.centroid(d) + ")";  
              })
        .attr("text-anchor", "middle")                    
        .text(function(d, i) { return data[i].label; });  
}

function drawBars(station)
{
    
    var trainL = station.trainLetters;

    var maxLateness = calculateMaxAverage(trainL);
    var trainsLen = Object.keys(trainL).length;
    var bWidth = 15;
    var bHeight = 250;
    var svgHeight = 300;
    var svgWidth = bWidth * trainsLen - 1 + 50;

    var ticksAmount = maxLateness / 10;
    var tickGap = Math.round(maxLateness / ticksAmount);

    var x = d3.scale.linear()
        .domain([0, 1])
        .range([0, bWidth]);

    var y = d3.scale.linear()
        .domain([0, maxLateness])
        .rangeRound([0, bHeight]);
    
    
    var sLatenessesOnBars = d3.select("article").append("svg")
        .attr("class", "bars")
        .attr("width", svgWidth)
        .attr("height", svgHeight);

    sLatenessesOnBars.selectAll("rect")
        .data(Object.keys(trainL))
        .enter().append("rect")
        .attr("x", function(d, i) { return i * bWidth; })
        .attr("y", function(d) { return svgHeight - y(trainL[d].latenessAverage); })
        .attr("width", bWidth)
        .attr("height", function(d) { return y(trainL[d].latenessAverage); })
        .on("mouseover", function(d) {toInfoBox(trainL[d], "Train");})
        .on("mouseout", function(d) {killInfoBox();});

    sLatenessesOnBars.selectAll("text")
        .data(Object.keys(trainL))
        .enter().append("text")
        .attr("x", function(d, i) {return i * bWidth;})
        .attr("y", function(d) {return svgHeight - y(trainL[d].latenessAverage);})
        .attr("dx", "0.2em")
        .attr("dy", "-0.4em")
        .attr("font-size", "0.8em")
        // .attr("text-anchor", "middle")
        .attr("stroke-width", "0")
        .text(function(d) {return trainL[d].title;});
    
    sLatenessesOnBars.selectAll("line")
        .data(y.ticks(ticksAmount))
        .enter().append("line")
        .attr("x1", 0)
        .attr("x2", svgWidth)
        .attr("y1", function(d, i) { return svgHeight - y(d); })
        .attr("y2", function(d, i) { return svgHeight - y(d); })
        .style("stroke", "#000000")
        .style("stroke-width", "0.6");

    sLatenessesOnBars.selectAll(".rule")
        .data(y.ticks(ticksAmount))
        .enter().append("text")
        .attr("class", "rule")
        .attr("x", svgWidth - 30)
        .attr("y", function (d, i) { return svgHeight - y(d); })
        .attr("dy", -3)
        .attr("stroke-width", "0")
        .text(function (d, i) { return  10*i; });
    
}

function stationLatenessesToBars(station)
{
    var sTrains = station.trains;
    var maxLateness = calculateMaxLateness(sTrains);
    var trainsLen = Object.keys(sTrains).length;
    var latenesses = [];
    var bWidth = 15;
    var bHeight = 180;
    
    var svgHeight = 200 + 40;
    var svgWidth = bWidth * trainsLen - 1 + 50;

    var ticksAmount = maxLateness / 60;

    var x = d3.scale.linear()
        .domain([0, 1])
        .range([0, bWidth]);

    var y = d3.scale.linear()
        .domain([0, maxLateness])
        .rangeRound([0, bHeight]);

    var sLatenessesOnBars = d3.select("article").append("svg")
        .attr("class", "bars")
        .attr("width", svgWidth)
        .attr("height", svgHeight);

    sLatenessesOnBars.selectAll("rect")
        .data(Object.keys(sTrains))
        .enter().append("rect")
        .attr("x", function(d, i) { return i * bWidth; })
        .attr("y", function(d) { return svgHeight - y(sTrains[d].lateness); })
        .attr("width", bWidth)
        .attr("height", function(d) { return y(sTrains[d].lateness); })
        .on("mouseover", function(d) {toInfoBox(sTrains[d], "Train");})
        .on("mouseout", function(d) {killInfoBox();})
    ;

    sLatenessesOnBars.selectAll("text")
        .data(Object.keys(sTrains))
        .enter().append("text")
        .attr("x", function(d, i) {return i * bWidth;})
        .attr("y", function(d) {return svgHeight - y(sTrains[d].lateness);})
        .attr("dx", "0em")
        .attr("dy", "-0.4em")
        .attr("font-size", "0.8em")
        .text(function(d) {return sTrains[d].title;});
    
    sLatenessesOnBars.selectAll("line")
        .data(y.ticks(ticksAmount))
        .enter().append("line")
        .attr("x1", 0)
        .attr("x2", svgWidth)
        .attr("y1", function(d, i) { return svgHeight - y(i * 60); })
        .attr("y2", function(d, i) { return svgHeight - y(i * 60); })
        .style("stroke", "#000000");

    sLatenessesOnBars.selectAll(".rule")
        .data(y.ticks(ticksAmount))
        .enter().append("text")
        .attr("class", "rule")
        .attr("x", svgWidth - 30)
        .attr("y", function (d, i) { return svgHeight - y(i * 60); })
        .attr("dy", -3)
        // .attr("text-anchor", "middle")
        .text(function (d, i) { return i * 60; });
}
