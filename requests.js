function requestStationInfo(code)
{
    var req = new XMLHttpRequest();
    var stationObject;
    var parses = new DOMParser();
    
    req.onreadystatechange = function(){
        if (req.readyState === 4)  
            if (req.status == 200 || req.status == 304)
            {
                var stationXML = req.responseText;
                var parser = new DOMParser();
                var stationInfo = parser.parseFromString(stationXML, "text/xml");
                stationObject = new Station(stationInfo);
                console.log(stationObject.title);
            }
    };

    // false lopussa tarkoittaa, että pyyntö ei ole asynkroninen, ja siksi
    // JavaScriptin suoritus keskeytyy. Tämä pitää laittaa normaaliksi
    // myöhemmin, sillä muuten sivu jumittuu aina kun se odottaa tietoa.
    req.open("GET", "query.php?info=station&code=" + code, false);
    req.send(null);

    return stationObject;
}

function requestAllTrainsXML()
{
    var req = new XMLHttpRequest();
    var allTrainsXML;
    req.onreadystatechange = function(){
        if (req.readyState === 4)
            if (req.status == 200 || req.status == 304)
                allTrainsXML = req.responseText;
    };

    req.open("GET", "query.php?info=trains", false);
    req.send(null);

    return allTrainsXML;
}